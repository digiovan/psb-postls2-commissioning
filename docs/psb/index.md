## **Quadrupole/Corrector/Bendings magnets calibrations**

---

**Files** for the <b><font color='green'>PSB RINGs</font></b> can be downloaded in the table below:

| MAGNET TYPE | FILE LINK                                                                |
|-------------|--------------------------------------------------------------------------|
| Bendings    | [Click here](calibration/psb_bendings_calibration.xlsx)    |
| Quadrupoles | [Click here](calibration/psb_quadrupoles_calibration.xlsx) |
| Correctors  | [Click here](calibration/PSB_corrector_types_v3.docx)      |

---

**Files** for the <b><font color='blue'>PSB Extraction Lines</font></b> can be downloaded in the table below:

   * The file **Quadrupoles** contains the strength to current conversion for the different optics.
   * The file **Angles** contains the angle to current conversion.

| MAGNET TYPE | FILE LINK                                                                |
|-------------|--------------------------------------------------------------------------|
| Bendings    | [Click here](calibration/bt_btm_btp_bty_bendings_calibration.xlsx)       |
| Quadrupoles | [Click here](calibration/bt_btm_btp_bty_quadrupoles_calibration.xlsx)    |
| Correctors  | [Click here](calibration/bt_btm_btp_bty_correctors_calibration.xlsx)     |
| Angles      | [Click here](calibration/bt_btm_btp_bty_angles.xlsx)                     |

The information collected in this file is mainly gathered from the **LS2**
version of the [layoutDB](https://layout.cern.ch/){target=_blank}. The
integrated field at peak current and the peak current are obtained from [NORMA
DB](https://norma-db.web.cern.ch/){target=_blank}

---

## Power Converters Settings

The operational settings of the SIRIUS power converters in BHP room (PSB power house) are available [here](power_converters/Operational_settings_for_SIRIUS_in_BHP.xlsx)

