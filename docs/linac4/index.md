<!---
## **LBE Daily Run Planning**

The **daily planning of the LBE run** is accessible [here](https://docs.google.com/spreadsheets/d/1wv7wdbvQOO42XySlQ4fhpuOWdpJof2cpLEgtq8L6DBA/edit?usp=sharing)

---

## **Hardware Commissioning**

The **planning of the HW commissioning** (including **I**ndividual **S**ystem **T**ests or **IST**) is accessible [here](https://asm.cern.ch/asm?type=LN4%20LBE%20Run&schedule=LN4%20LBE%20Run&version=1.0&state=Draft&view=week){target=_blank}.

You can check the **status of the checklist** [here](https://op-webtools.web.cern.ch/machine_checkout/#/test/machine/linac4){target=_blank}.

---

-->

## Cavity Phasing

The result of the cavity phasing campaign in August 2020 is available [here](cavity_phasing/Linac4_cavity_phasing_20200817to20.xlsx)

## **Optics**

The repository of **L4T-LT-LTB-LBE-BI** optics models is current available in [git](https://gitlab.cern.ch/injector-optics/TransferLines/psb-injection/tree/LS2){target=_blank}.

JB Lallement produced the current **optics files** for the L4T-LT-LTB-LBE can be downloaded in the table below:

| MAGNET TYPE | FILE LINK                                                                |
|-------------|--------------------------------------------------------------------------|
| Bendings    | [Click here](optics/L4T_Bendings.png)    |
| Quadrupoles | [Click here](optics/Quad-settings-160MeV.xlsx) |


---

## **Quadrupole/Corrector/Bendings magnets calibrations**

The current **file** for the L4T-LT-LTB-LBE-BI can be downloaded in the table below:

| MAGNET TYPE | FILE LINK                                                                   |
|-------------|-----------------------------------------------------------------------------|
| Bendings    | [Click here](calibration/l4_l4t_lt_ltb_lbe_bi_bendings_calibration.xlsx)       |
| Quadrupoles | [Click here](calibration/l4_l4t_lt_ltb_lbe_bi_quadrupoles_calibration.xlsx) |
| Correctors  | [Click here](calibration/l4_l4t_lt_ltb_lbe_bi_correctors_calibration.xlsx)  |


The information collected in this file is mainly gathered from the **LS2**
version of the [layoutDB](https://layout.cern.ch/){target=_blank}. The
integrated field at peak current and the peak current are obtained from [NORMA
DB](https://norma-db.web.cern.ch/){target=_blank}

---

## **RF Power Measurements**

| YEAR/MONTH  | FILE LINK                                  |
|-------------|--------------------------------------------|
| 2019/10     | [Click here](RF_power/201910_RF_Power.png) |



---

## **Power Converters Min/Max Limits**

The most-updated **file** (courtesy of D. Nisbet) can be downloaded [here](power_converters/LINAC4_PC_properties_v1.xlsx).

??? settings "Additional Information"
     1. Note the special case for the L4L steerers:  They are configured to operate from +0.1A to +10A (or +20A for the MEBT) but they have a polarity control which allows the current to the magnet to be reversed. Thus the knob should be from +10A to -10A (however +0.1A to -0.1A will be rejected, defined by LIMITS.I.STANDBY).
     2. A reminder for the pulsed steerers (after L4L): They are now configured to operate from +14.6A to -14.6A however they will reject the range +0.1A to -0.1A (defined by the property LIMITS.PULSE.MIN). 


     The latest software (Class_62 and Class_63) supports transactional behaviour for compatible tools, so for example when sending out-of-range settings from YASP.
  
     MIDIDISCAP:
     LIMITS.I.POS[0] is now 14.6A 
     LIMITS.I.NEG[0] is now -14.6A 
      
     MAXIDISCAP
     LIMITS.I.POS[0] is now 90.8A  for all L4D, L4C, L4P systems
     LIMITS.I.POS[0] is now 175A, 185A or 200A for all L4L systems
     LIMITS.I.POS[0] remains 105A for all L4T systems (to be checked if 105A is eventually authorised for the L4D, L4C, L4P systems)
      
     APOLO - RBH
     LIMITS.I.POS[0] is now 650A
     LIMITS.I.NEG[0] is now -650A
      
     APOLO - RBV
     LIMITS.I.POS[0] is now 400A
     LIMITS.I.NEG[0] is now -400A
      
     LEBT DELTA STEERERS
     LIMITS.I.POS[0] is now 10A
      
     MEBT DELTA STEERERS
     LIMITS.I.POS[0] is still 20A as according to the design files this should be OK (however I understand the magnet thermal protection activates after some time at ~15A). 
      
      
     I have not yet included the LT and LTB systems - we need to check the settings for these. They should be available in the next few days.
 

---

